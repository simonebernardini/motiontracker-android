package com.justbit.motiontracker;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.justbit.motiontracker.datamodels.LogListItem;
import com.justbit.motiontracker.listadapters.LogListAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import roboguice.activity.RoboAppCompatActivity;
import roboguice.inject.InjectView;

public class LogListActivity extends RoboAppCompatActivity {

    @InjectView(R.id.toolbar)
    private Toolbar toolbar;
    @InjectView(R.id.dump_list)
    private ListView dumpList;

    private String dumpDirectory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_list);
        setSupportActionBar(this.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        String root = Environment.getExternalStorageDirectory().toString();
        this.dumpDirectory = root + "/MotionTracker";
        LayoutInflater mLayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LogListAdapter adapter = new LogListAdapter(mLayoutInflater, new ArrayList<LogListItem>());
        this.dumpList.setAdapter(adapter);
        this.dumpList.setOnItemClickListener(new FileEntryClickListener());
    }

    @Override
    protected void onResume() {
        super.onResume();
        File dumpDirFile = new File(this.dumpDirectory);
        if (dumpDirFile.isDirectory()) {
            File[] fileList = dumpDirFile.listFiles();
            List<LogListItem> adapterDataset = new ArrayList<>();
            for (File currentFile : fileList) {
                LogListItem item = new LogListItem(currentFile.getName(), currentFile);
                adapterDataset.add(item);
            }
            ((LogListAdapter)this.dumpList.getAdapter()).replaceDataset(adapterDataset);
        }
    }

    private class FileEntryClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ListAdapter adapter = LogListActivity.this.dumpList.getAdapter();
            if (adapter != null) {
                Object clickedItem = adapter.getItem(position);
                if (clickedItem instanceof LogListItem) {
                    LogListItem castItem = (LogListItem)clickedItem;
                    File clickedFile = castItem.getLogFile();
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"manuel.cugliari@justbit.it"});
                    Uri fileUri = Uri.fromFile(clickedFile);
                    emailIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
                    startActivity(emailIntent);
                }
            }
        }
    }
}
