package com.justbit.motiontracker.listadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.justbit.motiontracker.R;
import com.justbit.motiontracker.datamodels.LogListItem;

import java.util.List;

/**
 * Created by Simone on 08/03/2016.
 */
public class LogListAdapter extends BaseAdapter {
    private LayoutInflater mLayoutInflater;
    private List<LogListItem> mDataset;

    public LogListAdapter(LayoutInflater mLayoutInflater, List<LogListItem> mDataset) {
        this.mDataset = mDataset;
        this.mLayoutInflater = mLayoutInflater;
    }

    @Override
    public int getCount() {
        return this.mDataset.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mDataset.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.mDataset.indexOf(this.mDataset.get(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LogListViewHolder viewHolder = null;
        final LogListItem item = (LogListItem) getItem(position);
        if (convertView == null) {
            convertView = this.mLayoutInflater.inflate(R.layout.log_list_item, parent, false);
            viewHolder = new LogListViewHolder();
            viewHolder.filenameTextView = (TextView) convertView.findViewById(R.id.log_list_item_filename_textview);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (LogListViewHolder) convertView.getTag();
        }
        viewHolder.filenameTextView.setText(item.getFileName());
        return convertView;
    }

    private static class LogListViewHolder {
        private TextView filenameTextView;
    }

    public void replaceDataset(List<LogListItem> mDataset) {
        this.mDataset = mDataset;
        notifyDataSetChanged();
    }
}
