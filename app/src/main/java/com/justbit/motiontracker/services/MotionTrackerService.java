package com.justbit.motiontracker.services;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.widget.Toast;

import com.google.inject.Inject;
import com.justbit.localizer.db.DBLocalizerHelper;
import com.justbit.localizer.event.LocationEvent;
import com.justbit.localizer.event.ServiceStatusChangedEvent;
import com.justbit.localizer.localization.LocationHandler;
import com.justbit.motiontracker.R;
import com.justbit.motiontracker.events.InstantVelocityAvailableEvent;
import com.justbit.motiontracker.location.LocalizerConfiguration;
import com.justbit.motiontracker.receivers.MotionWakefulBroadcastReceiver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import roboguice.service.RoboService;

public class MotionTrackerService extends RoboService {
    private static final int NUM_SENSOR_LISTENED = 5;
    private static final int ACCELEROMETER_INDEX = 0;
    private static final int UNCALIBRATED_GYRO_INDEX = 1;
    private static final int CALIBRATED_GYRO_INDEX = 2;
    private static final int UNCALIBRATED_MAGNET_INDEX = 3;
    private static final int CALIBRATED_MAGNET_INDEX = 4;
    private final Sensor[] sensors = new Sensor[NUM_SENSOR_LISTENED];
    private final SensorEventListener[] sensorListeners = new SensorEventListener[NUM_SENSOR_LISTENED];
    private final SensorHandler[] sensorHandlers = new SensorHandler[NUM_SENSOR_LISTENED];
    public static final String EXTRA_SESSION_TYPE = "com.justbit.motiontracker.services.EXTRA_SESSION_TYPE";
    public static final String EXTRA_BASE_FILENAME = "com.justbit.motiontracker.services.EXTRA_BASE_FILENAME";
    public static final String EXTRA_SENSOR_ARRAY = "com.justbit.motiontracker.services.EXTRA_SENSOR_ARRAY";

    public enum SessionType {INDOOR, OUTDOOR}
    @Inject
    private LocationHandler locationHandler;
    @Inject
    private LocalizerConfiguration localizerConfiguration;
    @Inject
    private DBLocalizerHelper localizerHelper;

    private GPSHandler gpsTrackingHandler;
    private Intent startingIntent;
    private SensorManager sensorManager;
    private SessionType actualSessionType;
    private int[] selectedSensorsArray;


    public MotionTrackerService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        EventBus.getDefault().post(new ServiceStatusChangedEvent(ServiceStatusChangedEvent.ServiceStatus.STARTED));
        if (intent != null) {
            this.actualSessionType = (SessionType) intent.getSerializableExtra(EXTRA_SESSION_TYPE);
            this.selectedSensorsArray = intent.getIntArrayExtra(EXTRA_SENSOR_ARRAY);
            if (this.actualSessionType.equals(SessionType.OUTDOOR)) {
                String baseAccelerometerDumpFilename = intent.getStringExtra(EXTRA_BASE_FILENAME);
                EventBus.getDefault().register(this);
                initializeAndStartGpsHandling();
                //initializeAndStartAccelerometerHandling(this.sensorListeners[ACCELEROMETER_INDEX], baseAccelerometerDumpFilename);
                initializeAndStartSensorHandling(this.selectedSensorsArray, baseAccelerometerDumpFilename);
            }
            else if (this.actualSessionType.equals(SessionType.INDOOR)) {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.session_not_supported), Toast.LENGTH_SHORT).show();
                stopSelf();
            }
        }
        this.startingIntent = intent;
        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void startLocalizer() {
        locationHandler.start();
    }

    private void stopLocalizer() {
        locationHandler.stop();
        this.gpsTrackingHandler.sendEmptyMessage(GPSHandler.EXIT);
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        stopLocalizer();
        if (this.sensorManager != null) {
            for (int i = 0; i < NUM_SENSOR_LISTENED; i++) {
                SensorEventListener currentListener = this.sensorListeners[i];
                SensorHandler currentHandler = this.sensorHandlers[i];
                Sensor currentSensor = this.sensors[i];
                if (currentListener != null) {
                    this.sensorManager.unregisterListener(currentListener);
                    this.sensorListeners[i] = null;
                }
                if (currentHandler != null) {
                    currentHandler.sendEmptyMessage(SensorHandler.FINALIZE_DUMP);
                    this.sensorHandlers[i] = null;
                }
                if (currentSensor != null) {
                    this.sensors[i] = null;
                }
            }
        }
        MotionWakefulBroadcastReceiver.completeWakefulIntent(this.startingIntent);
        EventBus.getDefault().post(new ServiceStatusChangedEvent(ServiceStatusChangedEvent.ServiceStatus.STOPPED));
        super.onDestroy();
    }

    @Subscribe
    public void handle(LocationEvent event) {
        if (this.gpsTrackingHandler != null) {
            this.gpsTrackingHandler.sendEmptyMessage(GPSHandler.NEW_LOCATION_FIX);
        }
    }

    private final class GPSHandler extends Handler {
        public static final int NEW_LOCATION_FIX = 0;
        public static final int EXIT = 1;
        private long previousTimeMillis;
        private long actualTimeMillis;
        private double previousLat;
        private double previousLng;
        private double actualLat;
        private double actualLng;

        public GPSHandler(Looper looper) {
            super(looper);
            this.previousTimeMillis = System.currentTimeMillis();
            this.actualTimeMillis = this.previousTimeMillis;
            if (MotionTrackerService.this.localizerHelper != null && MotionTrackerService.this.localizerHelper.getLastLocation() != null) {
                Location currentLocation = MotionTrackerService.this.localizerHelper.getLastLocation().getLocation();
                this.previousLat = currentLocation.getLatitude();
                this.previousLng = currentLocation.getLongitude();
                this.actualLat = currentLocation.getLatitude();
                this.actualLng = currentLocation.getLongitude();
            }
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == NEW_LOCATION_FIX) {
                synchronized (this) {
                    if (MotionTrackerService.this.localizerHelper != null && MotionTrackerService.this.localizerHelper.getLastLocation() != null) {
                        Location currentLocation = MotionTrackerService.this.localizerHelper.getLastLocation().getLocation();
                        float speed = 0f;
                        if (currentLocation != null) {
                            this.previousLat = this.actualLat;
                            this.previousLng = this.actualLng;
                            this.previousTimeMillis = this.actualTimeMillis;
                            this.actualLat = currentLocation.getLatitude();
                            this.actualLng = currentLocation.getLongitude();
                            this.actualTimeMillis = System.currentTimeMillis();
                            if (currentLocation.hasSpeed() && currentLocation.getSpeed() > 1.5f) {
                                speed = currentLocation.getSpeed();
                            } else {
                                long motionDistanceInMeters = calculateDistance(this.previousLat, this.previousLng, this.actualLat, this.actualLng);
                                long timeDifferenceMillis = this.actualTimeMillis - this.previousTimeMillis;
                                speed = calculateVelocity(motionDistanceInMeters, timeDifferenceMillis);
                            }
                        }
                        InstantVelocityAvailableEvent event = new InstantVelocityAvailableEvent(convertMetersPerSecondToKilometersPerHour(speed));
                        EventBus.getDefault().postSticky(event);
                    }
                }
            }
            else if (msg.what == EXIT) {
                getLooper().quit();
            }
        }

        private long calculateDistance(double lat1, double lng1, double lat2, double lng2) {
            double dLat = Math.toRadians(lat2 - lat1);
            double dLon = Math.toRadians(lng2 - lng1);
            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                    + Math.cos(Math.toRadians(lat1))
                    * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                    * Math.sin(dLon / 2);
            double c = 2 * Math.asin(Math.sqrt(a));
            long distanceInMeters = Math.round(6371000 * c);
            return distanceInMeters;
        }

        private float calculateVelocity(long distanceInMeters, long timeDifferenceMillis) {
            long timeDifferenceSec = Math.round(timeDifferenceMillis / 1000f);
            float velocity = (float) distanceInMeters / (float) timeDifferenceSec;
            return velocity;
        }

        private float convertMetersPerSecondToKilometersPerHour(float metersPerSecond) {
            return metersPerSecond * 3.6f;
        }
    }

    private final class SensorHandler extends Handler {
        public static final int SENSOR_READING_AVAILABLE = 1;
        public static final int FINALIZE_DUMP = 2;
        public static final String SENSOR_READING_X_AXIS_EXTRA = "com.justbit.motiontracker.services.MotionTrackerService$SensorHandler.SENSOR_READING_X_AXIS_EXTRA";
        public static final String SENSOR_READING_Y_AXIS_EXTRA = "com.justbit.motiontracker.services.MotionTrackerService$SensorHandler.SENSOR_READING_Y_AXIS_EXTRA";
        public static final String SENSOR_READING_Z_AXIS_EXTRA = "com.justbit.motiontracker.services.MotionTrackerService$SensorHandler.SENSOR_READING_Z_AXIS_EXTRA";

        private PrintWriter fileWriter;

        public SensorHandler(Looper looper, String baseDumpFilename) {
            super(looper);
            Date now = new Date();
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss", Locale.getDefault());
            StringBuilder filenameBuilder =  new StringBuilder(baseDumpFilename + "_" + dateFormatter.format(now));
            filenameBuilder.append(".csv");
            String root = Environment.getExternalStorageDirectory().toString();
            File dumpDir = new File(root + "/MotionTracker");
            if (!dumpDir.exists()) {
                if (dumpDir.mkdirs()) {
                    File dumpFile  = new File(dumpDir.getAbsolutePath() + "/" + filenameBuilder.toString());
                    try {
                        this.fileWriter = new PrintWriter(new BufferedWriter(new FileWriter(dumpFile)));
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            else {
                File dumpFile  = new File(dumpDir.getAbsolutePath() + "/" + filenameBuilder.toString());
                try {
                    this.fileWriter = new PrintWriter(new BufferedWriter(new FileWriter(dumpFile)));
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == SENSOR_READING_AVAILABLE) {
                Bundle bundle = msg.getData();
                float xAxisReading = bundle.getFloat(SENSOR_READING_X_AXIS_EXTRA);
                float yAxisReading = bundle.getFloat(SENSOR_READING_Y_AXIS_EXTRA);
                float zAxisReading = bundle.getFloat(SENSOR_READING_Z_AXIS_EXTRA);
                StringBuilder lineBuilder  = new StringBuilder();
                lineBuilder.append(String.valueOf(xAxisReading))
                        .append(",")
                        .append(String.valueOf(yAxisReading))
                        .append(",")
                        .append(String.valueOf(zAxisReading));
                if (this.fileWriter != null) {
                    this.fileWriter.println(lineBuilder.toString());
                }
            }
            else if (msg.what == FINALIZE_DUMP) {
                if (this.fileWriter != null) {
                    this.fileWriter.flush();
                    this.fileWriter.close();
                    getLooper().quit();
                }
            }
        }
    }

    private class SensorListener implements SensorEventListener {

        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER || event.sensor.getType() == Sensor.TYPE_GYROSCOPE_UNCALIBRATED
                    || event.sensor.getType() == Sensor.TYPE_GYROSCOPE || event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED
                    || event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                float xAxisValue = event.values[0];
                float yAxisValue = event.values[1];
                float zAxisValue = event.values[2];
                Message msg = null;
                Bundle msgData = new Bundle();
                msgData.putFloat(SensorHandler.SENSOR_READING_X_AXIS_EXTRA, xAxisValue);
                msgData.putFloat(SensorHandler.SENSOR_READING_Y_AXIS_EXTRA, yAxisValue);
                msgData.putFloat(SensorHandler.SENSOR_READING_Z_AXIS_EXTRA, zAxisValue);
                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    msg = MotionTrackerService.this.sensorHandlers[ACCELEROMETER_INDEX].obtainMessage();
                    msg.what = SensorHandler.SENSOR_READING_AVAILABLE;
                    msg.setData(msgData);
                    MotionTrackerService.this.sensorHandlers[ACCELEROMETER_INDEX].sendMessage(msg);
                }
                else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
                    msg = MotionTrackerService.this.sensorHandlers[UNCALIBRATED_GYRO_INDEX].obtainMessage();
                    msg.what = SensorHandler.SENSOR_READING_AVAILABLE;
                    msg.setData(msgData);
                    MotionTrackerService.this.sensorHandlers[UNCALIBRATED_GYRO_INDEX].sendMessage(msg);
                }
                else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
                    msg = MotionTrackerService.this.sensorHandlers[CALIBRATED_GYRO_INDEX].obtainMessage();
                    msg.what = SensorHandler.SENSOR_READING_AVAILABLE;
                    msg.setData(msgData);
                    MotionTrackerService.this.sensorHandlers[CALIBRATED_GYRO_INDEX].sendMessage(msg);
                }
                else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
                    msg = MotionTrackerService.this.sensorHandlers[UNCALIBRATED_MAGNET_INDEX].obtainMessage();
                    msg.what = SensorHandler.SENSOR_READING_AVAILABLE;
                    msg.setData(msgData);
                    MotionTrackerService.this.sensorHandlers[UNCALIBRATED_MAGNET_INDEX].sendMessage(msg);
                }
                else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                    msg = MotionTrackerService.this.sensorHandlers[CALIBRATED_MAGNET_INDEX].obtainMessage();
                    msg.what = SensorHandler.SENSOR_READING_AVAILABLE;
                    msg.setData(msgData);
                    MotionTrackerService.this.sensorHandlers[CALIBRATED_MAGNET_INDEX].sendMessage(msg);
                }
                else {
                    throw new Error("Unsupported Sensor type");
                }
                com.justbit.motiontracker.events.SensorEvent sensorEvent = new com.justbit.motiontracker.events.SensorEvent(event.sensor.getType(), xAxisValue, yAxisValue, zAxisValue);
                EventBus.getDefault().post(sensorEvent);
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

    }

    private void initializeAndStartGpsHandling() {
        HandlerThread gpsTrackingThread = new HandlerThread("GPSTrackingThread", Process.THREAD_PRIORITY_DEFAULT);
        gpsTrackingThread.start();
        Looper gpsTrackingThreadLooper = gpsTrackingThread.getLooper();
        this.gpsTrackingHandler = new GPSHandler(gpsTrackingThreadLooper);
        startLocalizer();
    }

    private void initializeAndStartSensorHandling(int[] sensorTypes, String baseDumpFilename) {
        if (sensorTypes.length <= this.sensors.length) {
            for (int i = 0; i < sensorTypes.length; i++) {
                int currentSensorType = sensorTypes[i];
                Sensor currentSensor = this.sensorManager.getDefaultSensor(currentSensorType);
                if (currentSensor != null) {
                    HandlerThread sensorHandlerThread = null;
                    String handlerThreadName = null;
                    StringBuilder finalBaseDumpFilenameBuilder = new StringBuilder(baseDumpFilename);
                    int relativeIndex = -1;
                    if (currentSensorType == Sensor.TYPE_ACCELEROMETER) {
                        handlerThreadName = "AccelerometerDumpThread";
                        finalBaseDumpFilenameBuilder.append("_accelerometer");
                        relativeIndex = ACCELEROMETER_INDEX;
                    }
                    else if (currentSensorType == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
                        handlerThreadName = "UncalibratedGyroDumpThread";
                        finalBaseDumpFilenameBuilder.append("_uncalibrated_gyroscope");
                        relativeIndex = UNCALIBRATED_GYRO_INDEX;
                    }
                    else if (currentSensorType == Sensor.TYPE_GYROSCOPE) {
                        handlerThreadName = "CalibratedGyroDumpThread";
                        finalBaseDumpFilenameBuilder.append("_calibrated_gyroscope");
                        relativeIndex = CALIBRATED_GYRO_INDEX;
                    }
                    else if (currentSensorType == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
                        handlerThreadName = "UncalibratedMagnetDumpThread";
                        finalBaseDumpFilenameBuilder.append("_uncalibrated_magnetometer");
                        relativeIndex = UNCALIBRATED_MAGNET_INDEX;
                    }
                    else if (currentSensorType == Sensor.TYPE_MAGNETIC_FIELD) {
                        handlerThreadName = "CalibratedMagnetDumpThread";
                        finalBaseDumpFilenameBuilder.append("_calibrated_magnetometer");
                        relativeIndex = CALIBRATED_MAGNET_INDEX;
                    }
                    if (handlerThreadName != null) {
                        this.sensors[relativeIndex] = currentSensor;
                        sensorHandlerThread = new HandlerThread(handlerThreadName, Process.THREAD_PRIORITY_BACKGROUND);
                        sensorHandlerThread.start();
                        Looper sensorHandlerThreadLooper = sensorHandlerThread.getLooper();
                        this.sensorHandlers[relativeIndex] = new SensorHandler(sensorHandlerThreadLooper, finalBaseDumpFilenameBuilder.toString());
                        this.sensorListeners[relativeIndex] = new SensorListener();
                        this.sensorManager.registerListener(this.sensorListeners[relativeIndex], this.sensors[relativeIndex], SensorManager.SENSOR_DELAY_NORMAL);
                    }
                    else {
                        throw new Error("Unrecognized index");
                    }
                }
                else {
                    String sensorName = null;
                    if (i == Sensor.TYPE_ACCELEROMETER) {
                        sensorName = "Accelerometer";
                    }
                    else if (i == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
                        sensorName = "Uncalibrated Gyroscope";
                    }
                    else if (i == Sensor.TYPE_GYROSCOPE) {
                        sensorName = "Calibrated Gyroscope";
                    }
                    else if (i == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
                        sensorName = "Uncalibrated Magnetometer";
                    }
                    else if (i == Sensor.TYPE_MAGNETIC_FIELD) {
                        sensorName = "Calibrated Magnetometer";
                    }
                    if (sensorName != null) {
                        Toast.makeText(getApplicationContext(), sensorName + " unavailable", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        throw new Error("Unsupported sensor type");
                    }
                }
            }
        }
        else {
            throw new Error("Dishomogeneous arrays");
        }
    }

    private void initializeAndStartAccelerometerHandling(SensorEventListener sensorEventListener, String baseDumpFilename) {
        if (this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            this.sensors[ACCELEROMETER_INDEX] = this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            HandlerThread accelerometerDumpThread = new HandlerThread("AccelerometerDumpThread", Process.THREAD_PRIORITY_BACKGROUND);
            accelerometerDumpThread.start();
            Looper accelerometerDumpThreadLooper = accelerometerDumpThread.getLooper();
            this.sensorHandlers[ACCELEROMETER_INDEX] = new SensorHandler(accelerometerDumpThreadLooper, baseDumpFilename);
            if (this.sensorManager != null && this.sensors[ACCELEROMETER_INDEX] != null) {
                this.sensorManager.registerListener(sensorEventListener, this.sensors[ACCELEROMETER_INDEX], SensorManager.SENSOR_DELAY_NORMAL);
            }

        }
        else {
            Toast.makeText(getApplicationContext(), "Accelerometer unavailable", Toast.LENGTH_SHORT).show();
        }
    }
}
