package com.justbit.motiontracker.events;

/**
 * Created by Simone on 05/08/2016.
 */
public class SensorEvent {
    private int sensorType;
    private float xAxis;
    private float yAxis;
    private float zAxis;

    public SensorEvent(int sensorType, float xAxis, float yAxis, float zAxis) {
        this.sensorType = sensorType;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.zAxis = zAxis;
    }

    public int getSensorType() {
        return sensorType;
    }

    public void setSensorType(int sensorType) {
        this.sensorType = sensorType;
    }

    public float getxAxis() {
        return xAxis;
    }

    public void setxAxis(float xAxis) {
        this.xAxis = xAxis;
    }

    public float getyAxis() {
        return yAxis;
    }

    public void setyAxis(float yAxis) {
        this.yAxis = yAxis;
    }

    public float getzAxis() {
        return zAxis;
    }

    public void setzAxis(float zAxis) {
        this.zAxis = zAxis;
    }
}
