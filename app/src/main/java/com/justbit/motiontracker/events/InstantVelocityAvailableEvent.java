package com.justbit.motiontracker.events;

/**
 * Created by Simone on 20/01/2016.
 */
public class InstantVelocityAvailableEvent {
    private float velocity;

    public InstantVelocityAvailableEvent(float velocity) {
        this.velocity = velocity;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }
}
