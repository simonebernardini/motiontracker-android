package com.justbit.motiontracker.location;

import com.google.inject.Singleton;
import com.justbit.localizer.configurations.LocalizerBaseConfiguration;

/**
 * Created by Simone on 18/01/2016.
 */
@Singleton
public class LocalizerConfiguration extends LocalizerBaseConfiguration {
    private String deniedText = "Go Settings and Grant Permission";
    private String rationaleTitleText = "Permission required";
    private String rationaleText;
    private String rationaleOkText = "OK";
    private String rationaleNoText = "NO";

    public String getRationaleText() {
        return rationaleText;
    }

    public void setRationaleText(String rationaleText) {
        this.rationaleText = rationaleText;
    }

    public String getRationaleOkText() {
        return rationaleOkText;
    }

    public void setRationaleOkText(String rationaleOkText) {
        this.rationaleOkText = rationaleOkText;
    }

    public String getRationaleNoText() {
        return rationaleNoText;
    }

    public void setRationaleNoText(String rationaleNoText) {
        this.rationaleNoText = rationaleNoText;
    }

    public String getRationaleTitleText() {
        return rationaleTitleText;
    }

    public void setRationaleTitleText(String rationaleTitleText) {
        this.rationaleTitleText = rationaleTitleText;
    }

    public String getDeniedText() {
        return deniedText;
    }

    public void setDeniedText(String deniedText) {
        this.deniedText = deniedText;
    }
}
