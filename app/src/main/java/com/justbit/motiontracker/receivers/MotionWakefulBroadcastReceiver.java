package com.justbit.motiontracker.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.justbit.motiontracker.services.MotionTrackerService;

public class MotionWakefulBroadcastReceiver extends WakefulBroadcastReceiver {
    public static final String ACTION_START_MOTION_SERVICE = "com.justbit.motiontracker.receivers.ACTION_START_MOTION_SERVICE";
    public static final String ACTION_STOP_MOTION_SERVICE = "com.justbit.motiontracker.receivers.ACTION_STOP_MOTION_SERVICE";

    public MotionWakefulBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(ACTION_START_MOTION_SERVICE)) {
            // start wakeful service
            Intent motionTrackIntent = new Intent(context, MotionTrackerService.class);
            motionTrackIntent.putExtra(MotionTrackerService.EXTRA_SESSION_TYPE, intent.getSerializableExtra(MotionTrackerService.EXTRA_SESSION_TYPE));
            motionTrackIntent.putExtra(MotionTrackerService.EXTRA_BASE_FILENAME, intent.getStringExtra(MotionTrackerService.EXTRA_BASE_FILENAME));
            motionTrackIntent.putExtra(MotionTrackerService.EXTRA_SENSOR_ARRAY, intent.getIntArrayExtra(MotionTrackerService.EXTRA_SENSOR_ARRAY));
            startWakefulService(context, motionTrackIntent);
        }
        else if (intent.getAction().equals(ACTION_STOP_MOTION_SERVICE)) {
            Intent motionTrackStopIntent = new Intent(context, MotionTrackerService.class);
            context.stopService(motionTrackStopIntent);
        }
        else {
            throw new UnsupportedOperationException("Not yet implemented");
        }
    }
}
