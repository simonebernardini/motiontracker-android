package com.justbit.motiontracker;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.canelmas.let.AskPermission;
import com.canelmas.let.DeniedPermission;
import com.canelmas.let.Let;
import com.canelmas.let.RuntimePermissionListener;
import com.canelmas.let.RuntimePermissionRequest;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.justbit.localizer.event.ServiceStatusChangedEvent;
import com.justbit.motiontracker.events.InstantVelocityAvailableEvent;
import com.justbit.motiontracker.events.SensorEvent;
import com.justbit.motiontracker.receivers.MotionWakefulBroadcastReceiver;
import com.justbit.motiontracker.services.MotionTrackerService;
import com.justbit.motiontracker.util.Codes;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import roboguice.activity.RoboAppCompatActivity;
import roboguice.inject.InjectView;

public class MainActivity extends RoboAppCompatActivity implements RuntimePermissionListener {
    private static final int X_AXIS_COLOR = Color.BLUE;
    private static final int Y_AXIS_COLOR = Color.RED;
    private static final int Z_AXIS_COLOR = Color.GREEN;
    private static final int MAX_SAMPLES = 80;
    @InjectView(R.id.toolbar)
    private Toolbar toolbar;
    @InjectView(R.id.current_speed_value_textview)
    private TextView instantVelocityValueTextView;
    @InjectView(R.id.start_session_button)
    private Button startSessionButton;
    @InjectView(R.id.accelerometer_chart_layout)
    private RelativeLayout accelerometerChartLayout;
    @InjectView(R.id.accelerometer_x_axis_chart)
    private LineChart accelerometerXAxisChart;
    @InjectView(R.id.accelerometer_y_axis_chart)
    private LineChart accelerometerYAxisChart;
    @InjectView(R.id.accelerometer_z_axis_chart)
    private LineChart accelerometerZAxisChart;
    @InjectView(R.id.uncalibrated_gyroscope_chart_layout)
    private RelativeLayout uncalibratedGyroChartLayout;
    @InjectView(R.id.uncalibrated_gyroscope_x_axis_chart)
    private LineChart uncalibratedGyroXAxisChart;
    @InjectView(R.id.uncalibrated_gyroscope_y_axis_chart)
    private LineChart uncalibratedGyroYAxisChart;
    @InjectView(R.id.uncalibrated_gyroscope_z_axis_chart)
    private LineChart uncalibratedGyroZAxisChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setSupportActionBar(this.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        }
        this.startSessionButton.setOnClickListener(new StartSessionClickListener());
        this.accelerometerXAxisChart.setDragEnabled(true);
        this.accelerometerYAxisChart.setDragEnabled(true);
        this.accelerometerZAxisChart.setDragEnabled(true);
        this.uncalibratedGyroXAxisChart.setDragEnabled(true);
        this.uncalibratedGyroYAxisChart.setDragEnabled(true);
        this.uncalibratedGyroZAxisChart.setDragEnabled(true);
        this.accelerometerXAxisChart.setPinchZoom(true);
        this.accelerometerYAxisChart.setPinchZoom(true);
        this.accelerometerZAxisChart.setPinchZoom(true);
        this.uncalibratedGyroXAxisChart.setPinchZoom(true);
        this.uncalibratedGyroYAxisChart.setPinchZoom(true);
        this.uncalibratedGyroZAxisChart.setPinchZoom(true);
        this.accelerometerXAxisChart.getLegend().setEnabled(false);
        this.accelerometerYAxisChart.getLegend().setEnabled(false);
        this.accelerometerZAxisChart.getLegend().setEnabled(false);
        this.uncalibratedGyroXAxisChart.getLegend().setEnabled(false);
        this.uncalibratedGyroYAxisChart.getLegend().setEnabled(false);
        this.uncalibratedGyroZAxisChart.getLegend().setEnabled(false);
    }


    @Override
    protected void onResume() {
        super.onResume();
        decideButtonText();
    }

    @AskPermission({Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    private void decideButtonText() {
        if (isMyServiceRunning(MotionTrackerService.class)) {
            this.startSessionButton.setText(getResources().getString(R.string.stop_tracking));
        }
        else {
            this.startSessionButton.setText(getResources().getString(R.string.start_tracking));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Let.handle(this, requestCode, permissions, grantResults);
    }

    @Override
    public void onShowPermissionRationale(List<String> permissionList, final RuntimePermissionRequest permissionRequest) {
        String permissionContent = "";
        for (String permission : permissionList) {
            if (permission.equals(Manifest.permission.ACCESS_COARSE_LOCATION) || permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                permissionContent = getResources().getString(R.string.location_permission_rationale);
            }
            else if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE) || permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissionContent = getResources().getString(R.string.access_external_files_rationale);
            }
            MaterialDialog rationaleDialog = new MaterialDialog.Builder(this)
                    .content(permissionContent)
                    .positiveText(android.R.string.ok)
                    .negativeText(android.R.string.cancel)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            permissionRequest.retry();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                        }
                    }).build();
            rationaleDialog.show();
        }
    }

    @Override
    public void onPermissionDenied(List<DeniedPermission> deniedPermissionList) {
        final StringBuilder sb = new StringBuilder();

        for (DeniedPermission result : deniedPermissionList) {
            if (result.isNeverAskAgainChecked()) {
                sb.append("Never Show Again for " + result.getPermission());
                sb.append("\n");
            }
        }

        if (sb.length() != 0) {
            MaterialDialog deniedDialog = new MaterialDialog.Builder(this)
                    .content(sb.toString())
                    .cancelable(true)
                    .positiveText(android.R.string.ok)
                    .negativeText(android.R.string.cancel)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, 1);

                            dialog.dismiss();
                        }
                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    }).build();
            deniedDialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_session_type:
                final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                final boolean outdoorSession = prefs.getBoolean(Codes.SharedPreferences.BOOLEAN_SESSION_TYPE_OUTDOOR_KEY, true);
                int selectedIndex = outdoorSession ? 0 : 1;
                MaterialDialog sessionTypeDialog = new MaterialDialog.Builder(this)
                        .title(R.string.session_type_dialog_title)
                        .items(R.array.session_type_items)
                        .itemsCallbackSingleChoice(selectedIndex, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                SharedPreferences.Editor editor = prefs.edit();
                                boolean outdoorSessionSelected = which == 0;
                                editor.putBoolean(Codes.SharedPreferences.BOOLEAN_SESSION_TYPE_OUTDOOR_KEY, outdoorSessionSelected);
                                dialog.dismiss();
                                editor.apply();
                                return true;
                            }
                        })
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        }).build();
                sessionTypeDialog.show();
                return true;
            case R.id.menu_action_dump_list:
                if (isMyServiceRunning(MotionTrackerService.class)) {
                    Toast.makeText(this, getResources().getString(R.string.session_running_history_not_available), Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent historyIntent = new Intent(this, LogListActivity.class);
                    startActivity(historyIntent);
                }
                return true;
            case R.id.menu_action_preferences:
                Intent preferencesIntent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(preferencesIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe(threadMode = ThreadMode.MainThread, sticky = true)
    public void handle(InstantVelocityAvailableEvent event) {
        float velocity = event.getVelocity();
        EventBus.getDefault().removeStickyEvent(event);
        this.instantVelocityValueTextView.setText(String.format("%.2f", velocity));
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void handle(ServiceStatusChangedEvent event) {
        switch (event.getServiceStatus()) {
            case STARTED:
                this.startSessionButton.setText(getResources().getString(R.string.stop_tracking));
                break;
            case STOPPED:
                this.startSessionButton.setText(getResources().getString(R.string.start_tracking));
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MainThread)
    public void handle(SensorEvent event) {
        float xAxis = event.getxAxis();
        float yAxis = event.getyAxis();
        float zAxis = event.getzAxis();
        float[] chartGroupReadings = new float[] {xAxis, yAxis, zAxis};
        LineChart[] chartGroupToBeUpdated = null;
        switch (event.getSensorType()) {
            case Sensor.TYPE_ACCELEROMETER:
                if (this.accelerometerXAxisChart.getData() == null) {
                    ArrayList<Entry> accelerometerXAxisDataset = new ArrayList<>();
                    LineDataSet accelerometerXAxisData = new LineDataSet(accelerometerXAxisDataset, "");
                    accelerometerXAxisData.setDrawValues(false);
                    accelerometerXAxisData.setColor(X_AXIS_COLOR);
                    accelerometerXAxisData.setCircleColor(X_AXIS_COLOR);
                    accelerometerXAxisData.setCircleColorHole(X_AXIS_COLOR);
                    LineData accelerometerXAxisLineData = new LineData(accelerometerXAxisData);
                    this.accelerometerXAxisChart.setData(accelerometerXAxisLineData);
                }
                if (this.accelerometerYAxisChart.getData() == null) {
                    ArrayList<Entry> accelerometerYAxisDataset = new ArrayList<>();
                    LineDataSet accelerometerYAxisData = new LineDataSet(accelerometerYAxisDataset, "");
                    accelerometerYAxisData.setDrawValues(false);
                    accelerometerYAxisData.setColor(Y_AXIS_COLOR);
                    accelerometerYAxisData.setCircleColor(Y_AXIS_COLOR);
                    accelerometerYAxisData.setCircleColorHole(Y_AXIS_COLOR);
                    LineData accelerometerYAxisLineData = new LineData(accelerometerYAxisData);
                    this.accelerometerYAxisChart.setData(accelerometerYAxisLineData);
                }
                if (this.accelerometerZAxisChart.getData() == null) {
                    ArrayList<Entry> accelerometerZAxisDataset = new ArrayList<>();
                    LineDataSet accelerometerZAxisData = new LineDataSet(accelerometerZAxisDataset, "");
                    accelerometerZAxisData.setDrawValues(false);
                    accelerometerZAxisData.setColor(Z_AXIS_COLOR);
                    accelerometerZAxisData.setCircleColor(Z_AXIS_COLOR);
                    accelerometerZAxisData.setCircleColorHole(Z_AXIS_COLOR);
                    LineData accelerometerZAxisLineData = new LineData(accelerometerZAxisData);
                    this.accelerometerZAxisChart.setData(accelerometerZAxisLineData);
                }
                chartGroupToBeUpdated = new LineChart[] {this.accelerometerXAxisChart, this.accelerometerYAxisChart, this.accelerometerZAxisChart};
                break;
            case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                if (this.uncalibratedGyroXAxisChart.getData() == null) {
                    ArrayList<Entry> uncalibratedGyroXAxisDataset = new ArrayList<>();
                    LineDataSet uncalibratedGyroXAxisData = new LineDataSet(uncalibratedGyroXAxisDataset, "");
                    uncalibratedGyroXAxisData.setDrawValues(false);
                    uncalibratedGyroXAxisData.setColor(X_AXIS_COLOR);
                    uncalibratedGyroXAxisData.setCircleColor(X_AXIS_COLOR);
                    uncalibratedGyroXAxisData.setCircleColorHole(X_AXIS_COLOR);
                    LineData uncalibratedGyroXAxisLineData = new LineData(uncalibratedGyroXAxisData);
                    this.uncalibratedGyroXAxisChart.setData(uncalibratedGyroXAxisLineData);
                }
                if (this.uncalibratedGyroYAxisChart.getData() == null) {
                    ArrayList<Entry> uncalibratedGyroYAxisDataset = new ArrayList<>();
                    LineDataSet uncalibratedGyroYAxisData = new LineDataSet(uncalibratedGyroYAxisDataset, "");
                    uncalibratedGyroYAxisData.setDrawValues(false);
                    uncalibratedGyroYAxisData.setColor(Y_AXIS_COLOR);
                    uncalibratedGyroYAxisData.setCircleColor(Y_AXIS_COLOR);
                    uncalibratedGyroYAxisData.setCircleColorHole(Y_AXIS_COLOR);
                    LineData uncalibratedGyroYAxisLineData = new LineData(uncalibratedGyroYAxisData);
                    this.uncalibratedGyroYAxisChart.setData(uncalibratedGyroYAxisLineData);
                }
                if (this.uncalibratedGyroZAxisChart.getData() == null) {
                    ArrayList<Entry> uncalibratedGyroZAxisDataset = new ArrayList<>();
                    LineDataSet uncalibratedGyroZAxisData = new LineDataSet(uncalibratedGyroZAxisDataset, "");
                    uncalibratedGyroZAxisData.setDrawValues(false);
                    uncalibratedGyroZAxisData.setColor(Y_AXIS_COLOR);
                    uncalibratedGyroZAxisData.setCircleColor(Y_AXIS_COLOR);
                    uncalibratedGyroZAxisData.setCircleColorHole(Y_AXIS_COLOR);
                    LineData uncalibratedGyroZAxisLineData = new LineData(uncalibratedGyroZAxisData);
                    this.uncalibratedGyroZAxisChart.setData(uncalibratedGyroZAxisLineData);
                }
                chartGroupToBeUpdated = new LineChart[] {this.uncalibratedGyroXAxisChart, this.uncalibratedGyroYAxisChart, this.uncalibratedGyroZAxisChart};
                break;
        }
        updateSensorChart(chartGroupToBeUpdated, chartGroupReadings);
    }

    private class StartSessionClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            final Intent serviceIntent = new Intent(MainActivity.this, MotionWakefulBroadcastReceiver.class);
            boolean outdoorSessionSelected = false;
            if (!isMyServiceRunning(MotionTrackerService.class)) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                outdoorSessionSelected = prefs.getBoolean(Codes.SharedPreferences.BOOLEAN_SESSION_TYPE_OUTDOOR_KEY, true);
                MotionTrackerService.SessionType sessionType = outdoorSessionSelected ? MotionTrackerService.SessionType.OUTDOOR : MotionTrackerService.SessionType.INDOOR;
                serviceIntent.putExtra(MotionTrackerService.EXTRA_SESSION_TYPE, sessionType);
                serviceIntent.setAction(MotionWakefulBroadcastReceiver.ACTION_START_MOTION_SERVICE);

                //MainActivity.this.startSessionButton.setText(getResources().getString(R.string.stop_tracking));
            }
            else {
                serviceIntent.setAction(MotionWakefulBroadcastReceiver.ACTION_STOP_MOTION_SERVICE);
                //MainActivity.this.startSessionButton.setText(getResources().getString(R.string.start_tracking));
            }
            if (outdoorSessionSelected) {
                MaterialDialog fileBaseNameDialog = new MaterialDialog.Builder(MainActivity.this)
                        .title(R.string.dialog_filename_base_title)
                        .customView(R.layout.base_filename_dialog, false)
                        .positiveText(android.R.string.ok)
                        .negativeText(android.R.string.cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                View rootView = dialog.getCustomView();
                                EditText baseFileNameEditText = (EditText) rootView.findViewById(R.id.base_filename_dialog_basename_edittext);
                                String insertedBaseFilename = baseFileNameEditText.getText().toString();
                                serviceIntent.putExtra(MotionTrackerService.EXTRA_BASE_FILENAME, insertedBaseFilename);
                                dialog.dismiss();
                                MaterialDialog sensorsChoiceDialog = new MaterialDialog.Builder(MainActivity.this)
                                        .title(R.string.dialog_sensors_title)
                                        .items(R.array.sensors)
                                        .itemsCallbackMultiChoice(null, new MaterialDialog.ListCallbackMultiChoice() {
                                            @Override
                                            public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                                                // Cleanup
                                                MainActivity.this.accelerometerChartLayout.setVisibility(View.GONE);
                                                MainActivity.this.uncalibratedGyroChartLayout.setVisibility(View.GONE);
                                                MainActivity.this.accelerometerXAxisChart.clear();
                                                MainActivity.this.accelerometerYAxisChart.clear();
                                                MainActivity.this.accelerometerZAxisChart.clear();
                                                MainActivity.this.uncalibratedGyroXAxisChart.clear();
                                                MainActivity.this.uncalibratedGyroYAxisChart.clear();
                                                MainActivity.this.uncalibratedGyroZAxisChart.clear();

                                                // re-initialize
                                                int[] selectedSensors = new int[which.length];
                                                for (int i = 0; i < which.length; i++) {
                                                    if (text[i].equals(getResources().getString(R.string.accelerometer))) {
                                                        selectedSensors[i] = Sensor.TYPE_ACCELEROMETER;
                                                        MainActivity.this.accelerometerChartLayout.setVisibility(View.VISIBLE);
                                                    }
                                                    else if (text[i].equals(getResources().getString(R.string.uncalibrated_gyroscope))) {
                                                        selectedSensors[i] = Sensor.TYPE_GYROSCOPE_UNCALIBRATED;
                                                        MainActivity.this.uncalibratedGyroChartLayout.setVisibility(View.VISIBLE);
                                                    }
                                                    else if (text[i].equals(getResources().getString(R.string.calibrated_gyroscope))) {
                                                        selectedSensors[i] = Sensor.TYPE_GYROSCOPE;
                                                    }
                                                    else if (text[i].equals(getResources().getString(R.string.uncalibrated_magnetometer))) {
                                                        selectedSensors[i] = Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED;
                                                    }
                                                    else {
                                                        selectedSensors[i] = Sensor.TYPE_MAGNETIC_FIELD;
                                                    }
                                                }
                                                serviceIntent.putExtra(MotionTrackerService.EXTRA_SENSOR_ARRAY, selectedSensors);
                                                sendBroadcast(serviceIntent);
                                                return true;
                                            }
                                        })
                                        .positiveText(android.R.string.ok)
                                        .negativeText(android.R.string.cancel)
                                        .show();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        }).build();
                fileBaseNameDialog.show();
            }
            else {
                sendBroadcast(serviceIntent);
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void updateSensorChart(LineChart[] threeAxisSensorCharts, float[] threeAxisReadings) {
        for (int i = 0; i < threeAxisSensorCharts.length; i++) {
            List<ILineDataSet> chartDatasets = threeAxisSensorCharts[i].getData().getDataSets();
            if (chartDatasets.size() > 0) {
                ILineDataSet dataSet = chartDatasets.get(0);
                int lastEntryIndex = -1;
                if (dataSet.getEntryCount() > 0) {
                    lastEntryIndex = dataSet.getEntryCount() - 1;
                }
                if (lastEntryIndex == -1) {
                    dataSet.addEntry(new Entry(0, threeAxisReadings[i]));
                }
                else {
                    dataSet.addEntry(new Entry(lastEntryIndex + 1, threeAxisReadings[i]));
                }
                threeAxisSensorCharts[i].getData().notifyDataChanged();
                threeAxisSensorCharts[i].notifyDataSetChanged();
                threeAxisSensorCharts[i].setVisibleXRangeMaximum(MAX_SAMPLES);
                threeAxisSensorCharts[i].moveViewToX(dataSet.getEntryCount() - MAX_SAMPLES + 1);
                threeAxisSensorCharts[i].invalidate();
            }
        }
    }
}
