package com.justbit.motiontracker;

import android.app.Fragment;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.util.Log;

import roboguice.activity.RoboAppCompatActivity;

/**
 * Created by Simone on 05/08/2016.
 */
public class SettingsActivity extends RoboAppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.activity_settings);
        Fragment settingsFragment = SettingsFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.activity_settings_content, settingsFragment).commit();
    }

    public static class SettingsFragment extends PreferenceFragment {

        public static SettingsFragment newInstance() {
            Bundle args = new Bundle();
            SettingsFragment fragment = new SettingsFragment();
            fragment.setArguments(args);
            return fragment;
        }

        public SettingsFragment() {

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
        }
    }
}
