package com.justbit.motiontracker;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.justbit.localizer.db.DBLocalizerHelper;
import com.justbit.localizer.localization.LocationHandler;
import com.justbit.motiontracker.location.LocalizerConfiguration;
import com.justbit.motiontracker.util.Foreground;

import net.danlew.android.joda.JodaTimeAndroid;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;

/**
 * Created by Simone on 18/01/2016.
 */
public class MotionTrackerApplication extends MultiDexApplication {
    private static MotionTrackerApplication instance;

    static {
        RoboGuice.setUseAnnotationDatabases(false);
    }

    public MotionTrackerApplication() {
        MotionTrackerApplication.instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeFabric();
        JodaTimeAndroid.init(this);
        initializeLocalizer();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static Context getContext() {
        return MotionTrackerApplication.instance.getApplicationContext();
    }

    private void initializeLocalizer() {
        RoboInjector mInjector = RoboGuice.getInjector(this);
        LocalizerConfiguration localizerConfiguration = mInjector.getInstance(LocalizerConfiguration.class);
        localizerConfiguration.setDbName("locations");
        DBLocalizerHelper dbHelper = mInjector.getInstance(DBLocalizerHelper.class);
        dbHelper.setBaseConfiguration(localizerConfiguration);
        dbHelper.setContext(this);
        dbHelper.init();
        Foreground.get(this).addListener(foregroundListener);
    }

    static Foreground.Listener foregroundListener = new Foreground.Listener() {
        @Override
        public void onBecameForeground() {
            RoboInjector mInjector = RoboGuice.getInjector(getContext());
            LocationHandler locationHandler = mInjector.getInstance(LocationHandler.class);
            locationHandler.inForeground();
            locationHandler.start();
        }

        @Override
        public void onBecameBackground() {
            RoboInjector mInjector = RoboGuice.getInjector(getContext());
            LocationHandler locationHandler = mInjector.getInstance(LocationHandler.class);
            locationHandler.inBackground();
            locationHandler.start();
        }
    };

    private void initializeFabric() {

        ArrayList<Kit> kits = new ArrayList<Kit>();
        if (BuildConfig.CRASHLYTICS) {
            kits.add(new Crashlytics());
        }

        /*if(BuildConfig.TWITTER) {
            TwitterAuthConfig authConfig = new TwitterAuthConfig(BuildConfig.TWITTER_CONSUMER_KEY, BuildConfig.TWITTER_CONSUMER_SECRET);
            kits.add(new Twitter(authConfig));
            kits.add(new TweetComposer());
        }*/

        if(!kits.isEmpty()) {
            Kit []kitArray = kits.toArray(new Kit[kits.size()]);
            Fabric.with(this, kitArray);
        }
    }
}
