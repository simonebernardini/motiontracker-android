package com.justbit.motiontracker.datamodels;

import org.parceler.Parcel;

import java.io.File;

/**
 * Created by Simone on 08/03/2016.
 */
@Parcel(Parcel.Serialization.BEAN)
public class LogListItem {
    private String fileName;
    private File logFile;

    public LogListItem(String fileName, File logFile) {
        this.fileName = fileName;
        this.logFile = logFile;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public File getLogFile() {
        return logFile;
    }

    public void setLogFile(File logFile) {
        this.logFile = logFile;
    }
}
