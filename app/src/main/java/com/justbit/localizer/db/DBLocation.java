package com.justbit.localizer.db;

import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Build;

/**
 * Created by francescopace on 20/06/15.
 */
public class DBLocation {

//    private ArrayList<String> addresses;
    private String addresses;

    private String mProvider;
    private long mTime = 0;
    private long mElapsedRealtimeNanos = 0;
    private double mLatitude = 0.0;
    private double mLongitude = 0.0;
    private boolean mHasAltitude = false;
    private double mAltitude = 0.0f;
    private boolean mHasSpeed = false;
    private float mSpeed = 0.0f;
    private boolean mHasBearing = false;
    private float mBearing = 0.0f;
    private boolean mHasAccuracy = false;
    private float mAccuracy = 0.0f;
//    private Bundle mExtras = null;
    private boolean mIsFromMockProvider = false;

    // Cache the inputs and outputs of computeDistanceAndBearing
    // so calls to distanceTo() and bearingTo() can share work
    private double mLat1 = 0.0;
    private double mLon1 = 0.0;
    private double mLat2 = 0.0;
    private double mLon2 = 0.0;
    private float mDistance = 0.0f;
    private float mInitialBearing = 0.0f;

    public DBLocation(){}

    public DBLocation(String addresses) {
        this.addresses = addresses;
    }

    @SuppressLint("NewApi")
    public void setLocation(Location l) {
        mProvider = l.getProvider();
        mTime = l.getTime();
        mLatitude = l.getLatitude();
        mLongitude = l.getLongitude();
        mHasAltitude = l.hasAltitude();
        mAltitude = l.getAltitude();
        mHasSpeed = l.hasSpeed();
        mSpeed = l.getSpeed();
        mHasBearing = l.hasBearing();
        mBearing = l.getBearing();
        mHasAccuracy = l.hasAccuracy();
        mAccuracy = l.getAccuracy();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mElapsedRealtimeNanos = l.getElapsedRealtimeNanos();
            mIsFromMockProvider = l.isFromMockProvider();
        }
    }

    @SuppressLint("NewApi")
    public Location getLocation() {
        Location l = new Location(mProvider);

        l.setTime(mTime);
        l.setLatitude(mLatitude);
        l.setLongitude(mLongitude);
        l.setAltitude(mAltitude);
        l.setSpeed(mSpeed);
        l.setBearing(mBearing);
        l.setAccuracy(mAccuracy);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            l.setElapsedRealtimeNanos(mElapsedRealtimeNanos);
        }

        return l;
    }

    public String getAddresses() {
        return addresses;
    }

    public void setAddresses(String addresses) {
        this.addresses = addresses;
    }

    public String getmProvider() {
        return mProvider;
    }

    public void setmProvider(String mProvider) {
        this.mProvider = mProvider;
    }

    public long getmTime() {
        return mTime;
    }

    public void setmTime(long mTime) {
        this.mTime = mTime;
    }

    public long getmElapsedRealtimeNanos() {
        return mElapsedRealtimeNanos;
    }

    public void setmElapsedRealtimeNanos(long mElapsedRealtimeNanos) {
        this.mElapsedRealtimeNanos = mElapsedRealtimeNanos;
    }

    public double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public boolean ismHasAltitude() {
        return mHasAltitude;
    }

    public void setmHasAltitude(boolean mHasAltitude) {
        this.mHasAltitude = mHasAltitude;
    }

    public double getmAltitude() {
        return mAltitude;
    }

    public void setmAltitude(double mAltitude) {
        this.mAltitude = mAltitude;
    }

    public boolean ismHasSpeed() {
        return mHasSpeed;
    }

    public void setmHasSpeed(boolean mHasSpeed) {
        this.mHasSpeed = mHasSpeed;
    }

    public float getmSpeed() {
        return mSpeed;
    }

    public void setmSpeed(float mSpeed) {
        this.mSpeed = mSpeed;
    }

    public boolean ismHasBearing() {
        return mHasBearing;
    }

    public void setmHasBearing(boolean mHasBearing) {
        this.mHasBearing = mHasBearing;
    }

    public float getmBearing() {
        return mBearing;
    }

    public void setmBearing(float mBearing) {
        this.mBearing = mBearing;
    }

    public boolean ismHasAccuracy() {
        return mHasAccuracy;
    }

    public void setmHasAccuracy(boolean mHasAccuracy) {
        this.mHasAccuracy = mHasAccuracy;
    }

    public float getmAccuracy() {
        return mAccuracy;
    }

    public void setmAccuracy(float mAccuracy) {
        this.mAccuracy = mAccuracy;
    }

    public boolean ismIsFromMockProvider() {
        return mIsFromMockProvider;
    }

    public void setmIsFromMockProvider(boolean mIsFromMockProvider) {
        this.mIsFromMockProvider = mIsFromMockProvider;
    }

    public double getmLat1() {
        return mLat1;
    }

    public void setmLat1(double mLat1) {
        this.mLat1 = mLat1;
    }

    public double getmLon1() {
        return mLon1;
    }

    public void setmLon1(double mLon1) {
        this.mLon1 = mLon1;
    }

    public double getmLat2() {
        return mLat2;
    }

    public void setmLat2(double mLat2) {
        this.mLat2 = mLat2;
    }

    public double getmLon2() {
        return mLon2;
    }

    public void setmLon2(double mLon2) {
        this.mLon2 = mLon2;
    }

    public float getmDistance() {
        return mDistance;
    }

    public void setmDistance(float mDistance) {
        this.mDistance = mDistance;
    }

    public float getmInitialBearing() {
        return mInitialBearing;
    }

    public void setmInitialBearing(float mInitialBearing) {
        this.mInitialBearing = mInitialBearing;
    }
}
