package com.justbit.localizer.db;

import android.content.Context;
import android.location.Location;

import com.google.inject.Singleton;
import com.justbit.androidarsenal.core.DateTimeUtils;
import com.justbit.localizer.configurations.LocalizerBaseConfiguration;
import com.snappydb.DB;
import com.snappydb.SnappyDB;
import com.snappydb.SnappydbException;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class DBLocalizerHelper {

    private Context context;
    private DB db;
    private LocalizerBaseConfiguration baseConfiguration;

    //region LIFECYCLE

    public void init() {
        create();
    }

    public void create() {
        SnappyDB.Builder builder = new SnappyDB.Builder(context);
        builder.name(baseConfiguration.getDbName());
        if (baseConfiguration.getDbPath() != null) {
            builder.directory(baseConfiguration.getDbPath());
        }

        try {
            this.db = builder.build();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            db.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }

    public void destroy() {
        try {
            db.close();
        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region METHODS
    private void addLocation(String key, DBLocation dbLocation) {

        synchronized (db) {
            try {
                db.put(key, dbLocation);
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        }
    }

    public void setLastLocation(Location location, ArrayList<String> addresses) {
        DBLocation dbLocation = new DBLocation();
        dbLocation.setLocation(location);
        dbLocation.setAddresses(addresses.toString());
        addLocation("last", dbLocation);

        String time = DateTimeUtils.dateTimeToISO(new DateTime(location.getTime()));
        addLocation(time, dbLocation);
    }

    public DBLocation getLastLocation() {

        synchronized (db) {
            DBLocation dbLocation = null;
            try {
                boolean isLastKeyExist = db.exists("last");
                if (isLastKeyExist) {
                    dbLocation = db.getObject("last", DBLocation.class);
                }
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            return dbLocation;
        }
    }

    public DBLocation getLocation(String time) {

        synchronized (db) {
            DBLocation dbLocation = null;
            try {
                boolean isLastKeyExist = db.exists(time);
                if (isLastKeyExist) {
                    dbLocation = db.getObject(time, DBLocation.class);
                }
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
            return dbLocation;
        }
    }

    public List<DBLocation> getLocationBefore(String end) {

        DateTime dateTime = new DateTime();
        dateTime = dateTime.withDate(2015, 1, 1);

//        List<String> keysSet = new ArrayList<>();
        String[] keys = new String[0];
        try {
//            KeyIterator k = db.allKeysIterator();
//            while (k.hasNext()) {
//                String[] sss = k.next(100);
//                Collections.addAll(keysSet, sss);
//            }
            keys = db.findKeysBetween(DateTimeUtils.dateTimeToISO(dateTime), end);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        return getLocationsByKeys(keys);
    }

    public List<DBLocation> getLocationAfter(String start) {

        String end = DateTimeUtils.dateTimeToISO(DateTime.now());
        String[] keys = new String[0];
        try {
            keys = db.findKeysBetween(start, end);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        return getLocationsByKeys(keys);
    }

    public List<DBLocation> getLocationBetween(String start, String end) {
        String[] keys = new String[0];
        try {
            keys = db.findKeysBetween(start, end);
        } catch (SnappydbException e) {
            e.printStackTrace();
        }

        return getLocationsByKeys(keys);

    }

    private List<DBLocation> getLocationsByKeys(String[] keys) {
        ArrayList<DBLocation> locations = new ArrayList<>();
        for(String key: keys) {
            try {
                locations.add(db.getObject(key, DBLocation.class));
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        }

        return locations;
    }
    //endregion

    public DB getDb() {
        return db;
    }

    public void setBaseConfiguration(LocalizerBaseConfiguration baseConfiguration) {
        this.baseConfiguration = baseConfiguration;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
