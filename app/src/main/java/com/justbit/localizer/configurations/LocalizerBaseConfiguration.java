package com.justbit.localizer.configurations;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class LocalizerBaseConfiguration {

    private String dbName;
    private String dbPath;

    @Inject
    public LocalizerBaseConfiguration() {

    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbPath() {
        return dbPath;
    }

    public void setDbPath(String dbPath) {
        this.dbPath = dbPath;
    }

}
