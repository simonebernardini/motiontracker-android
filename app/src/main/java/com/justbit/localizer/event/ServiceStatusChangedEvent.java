package com.justbit.localizer.event;

/**
 * Created by Simone on 27/01/2016.
 */
public class ServiceStatusChangedEvent {
    public enum ServiceStatus {STARTED, STOPPED}
    private ServiceStatus serviceStatus;

    public ServiceStatusChangedEvent(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }
}
