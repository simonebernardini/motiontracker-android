package com.justbit.localizer.event;

/**
 * Created by francescopace on 20/06/15.
 */
public class LocationEvent {

    public static enum ACTION {
        UPDATE
    }

    public static enum CATEGORY {
        LAST_LOCATION
    }

    private ACTION action;
    private CATEGORY category;
    private String time;

    public LocationEvent(ACTION action, CATEGORY category) {
        this.action = action;
        this.category = category;
    }

    public LocationEvent(ACTION action, CATEGORY category, String time) {
        this(action, category);
        this.time = time;
    }

    public ACTION getAction() {
        return action;
    }

    public void setAction(ACTION action) {
        this.action = action;
    }

    public CATEGORY getCategory() {
        return category;
    }

    public void setCategory(CATEGORY category) {
        this.category = category;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
