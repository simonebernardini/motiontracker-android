package com.justbit.localizer.localization;

import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationAvailability;
import com.google.inject.Inject;
import com.justbit.androidarsenal.core.DateTimeUtils;
import com.justbit.localizer.db.DBLocalizerHelper;
import com.justbit.localizer.db.DBLocation;
import com.justbit.localizer.event.LocationEvent;

import org.joda.time.DateTime;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import roboguice.service.RoboIntentService;

public class LocationService extends RoboIntentService {

    @Inject
    LocationUtils locationUtils;

    @Inject
    DBLocalizerHelper dbHelper;

    @Inject
    LocationHandler locationHandler;

	public LocationService() {
		this("Location Service");
	}

	public LocationService(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {

        boolean overDistance = true;
        boolean overTime = true;
        boolean overAccuracy = true;

        Log.e("Localizer", "New location found");

        if(LocationAvailability.hasLocationAvailability(intent)) {
            LocationAvailability availability = LocationAvailability.extractLocationAvailability(intent);
            Log.d("Localizer", "Extracted");
        }

        Location location = intent.getParcelableExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED);
        if (location != null) {
            DBLocation oldDbLocation = dbHelper.getLastLocation(); //LocationUtils.getLocationFromPreference();
            if(oldDbLocation != null) {

                Location oldLocation = oldDbLocation.getLocation();

                Log.e("Localizer", "Old provider: " + oldLocation.getProvider());
                Log.e("Localizer", "Current provider: " + location.getProvider());

                // DISTANCE CHECK
                int displacementMT = locationHandler.isInForeground() ? LocationUtils.DISPLACEMENTS_METERS_FOREGROUND : LocationUtils.DISPLACEMENTS_METERS;
                float distance = location.distanceTo(oldLocation);
                Log.e("Localizer", "Displacements meters: " + displacementMT);
                Log.e("Localizer", "Displacements meters between: " + distance);
                if(distance <= displacementMT) {
                    Log.e("Localizer", "Displacements meters FALSE");
                    overDistance = false;
                }

                // ETA CHECK
                int displacementMS = locationHandler.isInForeground() ? LocationUtils.DISPLACEMENTS_MILLISECONDS_FOREGROUND : LocationUtils.DISPLACEMENTS_MILLISECONDS;
                long currentFixTime = location.getTime();
                long previousFixTime = oldLocation.getTime();
                Log.e("Localizer", "Displacements ms: " + displacementMS);
                Log.e("Localizer", "Displacements ms current: " + currentFixTime);
                Log.e("Localizer", "Displacements ms previous: " + previousFixTime);
                Log.e("Localizer", "Displacements ms current - previous: " + (currentFixTime - previousFixTime));
                if( (currentFixTime - previousFixTime) <= displacementMS) {
                    Log.e("Localizer", "Displacements ms FALSE");
                    overTime = false;
                }

                // ACCURACY CHECK
                float previousAccuracy = oldLocation.getAccuracy();
                float currentAccuracy = location.getAccuracy();
                Log.e("Localizer", "Accuracy current : " + currentAccuracy);
                Log.e("Localizer", "Accuracy previous : " + previousAccuracy);
                if(currentAccuracy <= previousAccuracy) {
                    Log.e("Localizer", "Accuracy FALSE");
                    overAccuracy = false;
                }
            }

            //Qui entra un gioco di priorità
            boolean changeLocation = false;
            if(overAccuracy) {
                changeLocation = true;
            }
            else {
                if(overDistance) {
                    changeLocation = true;
                }
                else {
                    if(overTime) {
                        changeLocation = true;
                    }
                }
            }

            Log.i("Localizer", "overAccuracy = " + overAccuracy);
            Log.i("Localizer", "overDistance = " + overDistance);
            Log.i("Localizer", "overTime = " + overTime);

            // Non è cambiata una ceppa
            if(!changeLocation) {
                return;
            }

            // Aggiorna le prefs
            final ArrayList<String> addresses = locationUtils.getAddress(LocationService.this, location);
            dbHelper.setLastLocation(location, addresses);

//            Needle.onMainThread().execute(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(LocationService.this, addresses.toString(), Toast.LENGTH_SHORT).show();
//                }
//            });


            // Callback per fare comme ti pare
            Intent intentCB = new Intent();
            intentCB.setAction(LocationReceiver.ACTION_NEW_LOCATION);
            intentCB.putExtra(LocationReceiver.EXTRA_LOCATION, location);
            sendBroadcast(intentCB);

            // EventBus per chi non deve chiedere mai
            DateTime dateTime = new DateTime(location.getTime());
            String time = DateTimeUtils.dateTimeToISO(dateTime);
            EventBus.getDefault().post(new LocationEvent(LocationEvent.ACTION.UPDATE, LocationEvent.CATEGORY.LAST_LOCATION, time));

        }

	}
}
