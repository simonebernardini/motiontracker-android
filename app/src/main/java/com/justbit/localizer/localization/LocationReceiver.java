package com.justbit.localizer.localization;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

public class LocationReceiver extends BroadcastReceiver {

    public static final String ACTION_NEW_LOCATION = "ACTION_NEW_LOCATION";
    public static final String EXTRA_LOCATION = "EXTRA_LOCATION";

    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;

        if (intent.getAction() != null && intent.getAction().equals(ACTION_NEW_LOCATION)) {
            if (intent.getExtras() != null) {
                Location location = intent.getParcelableExtra(EXTRA_LOCATION);
                if (location != null) {
                    newLocation(location);
                }
            }
        }

    }

    private void newLocation(Location location) {
        // TODO: Rilanciare intent?? Vedere come fare....

    }

}
