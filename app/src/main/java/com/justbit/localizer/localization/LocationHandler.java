package com.justbit.localizer.localization;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.justbit.localizer.configurations.LocalizerBaseConfiguration;


@Singleton
public class LocationHandler implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    // A request to connect to Location Services
    private LocationRequest mLocationRequest;

    // Stores the current instantiation of the location client in this object
    private GoogleApiClient mLocationClient;

    // Pending intent for service
    PendingIntent mPendingIntent;

    private boolean isInForeground = true;
    private boolean mResolvingError = false;

    private Context context;
//    private Activity activity;
    private LocalizerBaseConfiguration baseConfiguration;

    @Inject
    public LocationHandler(Context context, LocalizerBaseConfiguration baseConfiguration) {
        this.context = context;
        this.baseConfiguration = baseConfiguration;
        init();
    }

    private void init() {

        Log.w("Localizer", "init");

        // Create a new location client, using the enclosing class to handle callbacks.
        mLocationClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Create a new global location parameters object
        mLocationRequest = LocationRequest.create();

        // Set the update interval
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        // Set the smallest displacement between locations updates
        mLocationRequest.setSmallestDisplacement(LocationUtils.DISPLACEMENTS_METERS);

        // Set the interval ceiling to one minute
        //mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        // Pending intent for background service
        mPendingIntent = PendingIntent.getService(context, 1, new Intent(context, LocationService.class), 0);
    }

    public void start(Activity activity) {
        Log.w("Localizer", "startUpdatesWithActivity");
//        this.activity = activity;
//        if(activity instanceof ActivityCompat.OnRequestPermissionsResultCallback) {
//            start();
//            Log.d("Localizer", "Ok! Activity implements the OnRequestPermissionsResultCallback method");
//            return;
//        }
//        Log.e("Localizer", "Your activity does not implements OnRequestPermissionsResultCallback method");
        /*try {
            ActivityCompat.OnRequestPermissionsResultCallback callback = (ActivityCompat.OnRequestPermissionsResultCallback) activity;
            Log.d("Localizer", "Ok! Activity implements the OnRequestPermissionsResultCallback method");
        }
        catch(Exception e) {
            Log.e("Localizer", "Your activity does not implements OnRequestPermissionsResultCallback method");
            return;
        }*/


    }

    public void start() {
        if (!mLocationClient.isConnected()) {
            mLocationClient.connect();
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                    // Request missing location permission.
//                    if (activity != null && (rationaleDialog == null || !rationaleDialog.isShowing()) ) {
//                        if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
//                            rationaleDialog = new MaterialDialog.Builder(this.activity)
//                                    .content(baseConfiguration.getRationaleText())
//                                    .positiveText(baseConfiguration.getRationaleOkText())
//                                    .negativeText(baseConfiguration.getRationaleNoText())
//                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                        @Override
//                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LocationUtils.REQUEST_CODE_LOCATION);
//                                            activity = null;
//                                        }
//                                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
//                                @Override
//                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                                    .show();
//                        } else {
//                            ActivityCompat.requestPermissions(this.activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LocationUtils.REQUEST_CODE_LOCATION);
//                            activity = null;
//                        }
//                    }
                    return;
                }
            }

            // Location permission has been granted, continue as usual.
            LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, mPendingIntent);
            LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, mPendingIntent);
        }
    }

    public void stop() {

        Log.w("Localizer", "stopUpdates");

        // If the client is connected
        if (mLocationClient.isConnected()) {
            mLocationClient.disconnect();
        }
    }

    public void inForeground() {
        mLocationRequest = LocationRequest.create();

        // Set the update interval
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS_FOREGROUND);

        // Use accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the smallest displacement between locations updates
        mLocationRequest.setSmallestDisplacement(LocationUtils.DISPLACEMENTS_METERS_FOREGROUND);

        // Set the interval ceiling to one minute
        //mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS_FOREGROUND);

        isInForeground = true;
    }

    public void inBackground() {
        mLocationRequest = LocationRequest.create();

        // Set the update interval
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        // Set the smallest displacement between locations updates
        mLocationRequest.setSmallestDisplacement(LocationUtils.DISPLACEMENTS_METERS);

        // Set the interval ceiling to one minute
        //mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        isInForeground = false;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.w("Localizer", "onConnected");
        if (mLocationClient.isConnected()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                    if (activity != null && (rationaleDialog == null || !rationaleDialog.isShowing()) ) {
//                        if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
//                            rationaleDialog = new MaterialDialog.Builder(this.activity)
//                                    .content(baseConfiguration.getRationaleText())
//                                    .positiveText(baseConfiguration.getRationaleOkText())
//                                    .negativeText(baseConfiguration.getRationaleNoText())
//                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                        @Override
//                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LocationUtils.REQUEST_CODE_LOCATION);
//                                        }
//                                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
//                                @Override
//                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                    dialog.dismiss();
//                                }
//                            })
//                                    .show();
//                        } else {
//                            ActivityCompat.requestPermissions(this.activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LocationUtils.REQUEST_CODE_LOCATION);
//                        }
//                    }
                    return;
                }
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, mPendingIntent);
        }
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to attempt to re-establish the connection.
        Log.i("Localizer", "Connection suspended");
        mLocationClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in onConnectionFailed.
        Log.i("Localizer", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

//        if (activity != null) {
//            GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//            int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
//            if (resultCode != ConnectionResult.SUCCESS) {
//                if (apiAvailability.isUserResolvableError(resultCode)) {
//                    apiAvailability.getErrorDialog(activity, resultCode, 18282).show();
//                } else {
//                    Log.i("GCMManager", "This device is not supported.");
//                }
//            }
//        }
    }

    public boolean isInForeground() {
        return isInForeground;
    }

}
