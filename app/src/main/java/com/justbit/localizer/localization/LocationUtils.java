/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.justbit.localizer.localization;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Defines app-wide constants and utilities
 */

public final class LocationUtils {

	/*
	 * Define a request code to send to Google Play services This code is returned in Activity.onActivityResult
	 */
	public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	public final static int REQUEST_CODE_LOCATION = 15;

	/*
	 * Constants for location update parameters
	 */
	// Milliseconds per second
	public static final int MILLISECONDS_PER_SECOND = 1000;

	// The update interval
	public static final int UPDATE_INTERVAL_IN_SECONDS = 10;
    public static final int UPDATE_INTERVAL_IN_SECONDS_FOREGROUND = 2;
	
	// The distance interval
	public static final int DISPLACEMENTS_METERS = 10;
    public static final int DISPLACEMENTS_METERS_FOREGROUND = 1;

    // The distance interval
    public static final int DISPLACEMENTS_MILLISECONDS = 1000 * 60 * 5;
    public static final int DISPLACEMENTS_MILLISECONDS_FOREGROUND = 1000 * 60 * 2;

	// A fast interval ceiling
	public static final int FAST_CEILING_IN_SECONDS = 10;
    public static final int FAST_CEILING_IN_SECONDS_FOREGROUND = 7;

	// Update interval in milliseconds
	public static final long UPDATE_INTERVAL_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS_FOREGROUND = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS_FOREGROUND;

	// A fast ceiling of update intervals, used when the app is visible
	public static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * FAST_CEILING_IN_SECONDS;
    public static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS_FOREGROUND = MILLISECONDS_PER_SECOND * FAST_CEILING_IN_SECONDS_FOREGROUND;

	// Create an empty string for initializing strings
	public static final String EMPTY_STRING = new String();

	/**
	 * Get the latitude and longitude from the Location object returned by
	 * Location Services.
	 //*
	 * @param currentLocation
	 *            A Location object containing the current location
	 * @return The latitude and longitude of the current location, or null if no
	 *         location is available.
	 */
	public String getLatLng(Context context, Location currentLocation) {
		// If the location is valid
		if (currentLocation != null) {

			// Return the latitude and longitude as strings
			//return context.getString(R.string.latitude_longitude, currentLocation.getLatitude(), currentLocation.getLongitude());
			return String.format("%1$.8f, %2$.8f", currentLocation.getLatitude(), currentLocation.getLongitude());
		} else {

			// Otherwise, return the empty string
			return EMPTY_STRING;
		}
	}

    public ArrayList<String> getAddress(Context context, Location location) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        // Create a list to contain the result address
        ArrayList<String> addressFragments = new ArrayList<>();
        List<Address> addresses = null;

        // Try to get an address for the current location. Catch IO or
        // network problems.
        try {

            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            // Catch network or other I/O problems.
        } catch (IOException exception1) {

            // print the stack trace
            exception1.printStackTrace();

            // Return an error message
            addressFragments.add("Exception in geocoder");
            return addressFragments;

            // Catch incorrect latitude or longitude values
        } catch (IllegalArgumentException exception2) {

            // Construct a message containing the invalid arguments
            String errorString = String.format("Illegal arguments: Latitude %1$.8f Longitude %2$.8f", location.getLatitude(), location.getLongitude());
            // Log the error and print the stack trace
            exception2.printStackTrace();

            //
            addressFragments.add(errorString);
            return addressFragments;
        }
        // If the reverse geocode returned an address
        if (addresses != null && addresses.size() > 0) {

            Address address = addresses.get(0);

            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }

            // Return the text
            return addressFragments;

            // If there aren't any addresses, post a message
        } else {
            addressFragments.add("Unknown address");
            return addressFragments;
        }
    }

}
